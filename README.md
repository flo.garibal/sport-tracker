# sport-tracker

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Features 
- [X] A wonderful map with two markers

### Roadmap
- [X] Map displayed
- [ ] Create menu on top of the map with fake menu entry
- [ ] Fix Language menu under the map
- [ ] Develop API calls
- [ ] Calls API from front
- [ ] Use flask to redirect front API to API adding the API key
- [ ] Create menu to load *.gpx* files
- [ ] Compute path grom *.gpx* and draw it
- [ ] Compute path distance
- [ ] Get path elevation
- [ ] Compute path cumulative elevation
- [ ] Personalize display (line string, waypoints, etc.)
- [ ] Display multiple *.gpx* on a same map
- [ ] Show/Hide some path from a list
- [ ] Create an account
- [ ] Save path in DB 
- [ ] Add infos about the path (duration, extra notes)  

 

