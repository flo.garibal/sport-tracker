import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import i18n from '@/plugins/i18n';
import { CHANGE_LOCALE } from '@/store/actions';
import { UPDATE_LOCALE } from '@/store/mutations';
import moduleMap from './map';

export default new Vuex.Store({
  state: {
    locale: i18n.locale as string,
  },
  mutations: {
    [UPDATE_LOCALE](state, { newLocale }) {
      state.locale = newLocale;
    },
  },
  actions: {
    [CHANGE_LOCALE]({ commit }, { newLocale }) {
      i18n.locale = newLocale;
      commit(UPDATE_LOCALE, { newLocale });
    },
  },
  plugins: [createPersistedState()],
  modules: {
    map: moduleMap,
  },
});
