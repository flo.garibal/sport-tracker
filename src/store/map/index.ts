import Vue from 'vue';
import Vuex from 'vuex';
import {
  Marker,
  PolyLine,
} from '@/types/map';

Vue.use(Vuex);

export default {
  namespaced: true,
  state: {
    markers: [] as Marker[],
    polyLines: [] as PolyLine[],
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  },
};
