import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';


import Home from '@/views/Home.vue';
import store from '@/store';
import { CHANGE_LOCALE } from '@/store/actions';
import { languages } from '@/plugins/i18n';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'root',
    beforeEnter(to, from, next) {
      next(store.state.locale);
    },
  },
  {
    path: '/:lang',
    component: Home,
    beforeEnter(to, from, next) {
      const { params: { lang } } = to;
      if (languages.includes(lang)) {
        if (store.state.locale !== lang) {
          store.dispatch(CHANGE_LOCALE, { newLocale: lang })
            .then();
        }
        return next();
      }
      return next({ path: store.state.locale });
    },
    // Routes must be declared below to be localized correctly
    // children: [
    //   {
    //     path: '',
    //     name: 'home',
    //     component: Home,
    //   },
    // ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
