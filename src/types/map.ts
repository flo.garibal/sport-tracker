import { LatLng } from 'leaflet';

export interface Marker {
  coordinates: LatLng;
  name?: string;
  fill?: boolean;
  fillColor?: string;
  fillOpacity?: number;
  opacity?: number;
  radius?: number;
  visible?: boolean;
}

export interface PolyLine {
  points: LatLng[];
  color?: string;
  name?: string;
  visible?: boolean;
  opacity?: number;
}
