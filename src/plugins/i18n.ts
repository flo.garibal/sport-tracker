import Vue from 'vue';
import VueI18n, { LocaleMessages } from 'vue-i18n';
import customDateTimeFormat from '@/locales/formats';

Vue.use(VueI18n);

const loadLocaleMessages = (): LocaleMessages => {
  const locales = require.context('../locales', true, /[A-Za-z0-9-_,\s]+\.json$/i);
  const messages: LocaleMessages = {};
  locales.keys().forEach((key) => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i);
    if (matched && matched.length > 1) {
      const locale = matched[1];
      messages[locale] = locales(key);
    }
  });
  return messages;
};

const getSupportedLanguages = (): { locale: string; displayableName: string }[] => {
  const languages = Object.getOwnPropertyNames(loadLocaleMessages());
  return languages.map((lang: string) => {
    const locale = lang;
    let displayableName: string;
    switch (lang.substr(0, 2)) {
      case 'fr':
        displayableName = 'French';
        break;
      case 'en':
        displayableName = 'English';
        break;
      default:
        displayableName = lang;
    }
    return { locale, displayableName };
  });
};

const checkDefaultLanguage = (): string | undefined => {
  let matched: string | undefined;
  const languages = Object.getOwnPropertyNames(loadLocaleMessages());
  const navigatorLang = navigator.language;
  languages.forEach((lang) => {
    // Check exact same locale (region included such as 'en-GB')
    if (lang === navigatorLang) {
      matched = lang;
    }
  });
  if (!matched) {
    languages.forEach((lang) => {
      // Check for non region specific locale (ex: 'en') against our non specific region lang
      const languagePartials = navigatorLang.split('-')[0];
      if (lang === languagePartials) {
        matched = lang;
      }
    });
  }
  if (!matched) {
    languages.forEach((lang) => {
      // Check if we have a close language in our messages
      // If the navigator lang is 'fr-CA' and we have 'fr' we do not want to display it in 'en' but
      // rather pick 'fr' lang
      const languagePartials = navigatorLang.split('-')[0];
      if (lang.split('-')[0] === languagePartials) {
        matched = lang;
      }
    });
  }
  return matched;
};

export {
  getSupportedLanguages,
};

export const languages = Object.getOwnPropertyNames(loadLocaleMessages());

export default new VueI18n({
  dateTimeFormats: customDateTimeFormat,
  locale: checkDefaultLanguage() || process.env.VUE_APP_I18N_LOCALE || 'en',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages: loadLocaleMessages(),
});
